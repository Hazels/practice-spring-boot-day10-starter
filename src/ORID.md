# Daily Report (2023/07/21)
# O (Objective): 
## 1. What did we learn today?
### We learned SpringBoot mapper and Flyway today.

## 2. What activities did you do? 
### I conducted a code review with my team members and .

## 3. What scenes have impressed you?
### I have been impressed by the integration testing today.
---------------------

# R (Reflective): 
## Please use one word to express your feelings about today's class.
---------------------
### Nervous.

# I (Interpretive):
## What do you think about this? What was the most meaningful aspect of this activity?
---------------------
### I think the most meaningful aspect of this activity is the retro with the team members and teachers today. Because This has made our team better understand each other and promoted team harmony.


# D (Decisional): 
## Where do you most want to apply what you have learned today? What changes will you make?
---------------------
### I will use Java stream instead of loop and try to reduce the use of if else statements as much as possible when I write the functions, and I will use the h2 virtual database in my project.
### I will develop a good coding habit of committing step by step and I still need to exercise my speech skills.
### I need to preview next week's React content well because I haven't used it before and I'm a bit worried that I won't be able to learn it well.




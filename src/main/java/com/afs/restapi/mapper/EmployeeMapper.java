package com.afs.restapi.mapper;

import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.entity.Employee;
import org.springframework.beans.BeanUtils;

public class EmployeeMapper {
    public static Employee toEntity(EmployeeCreateRequest employeeCreateRequest) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeCreateRequest,employee);
        return employee;
    }

    public static Employee toEntity(EmployeeResponse employeeResponse) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeResponse,employee);
        return employee;
    }

    public static EmployeeResponse toResponse(Employee employee) {
        EmployeeResponse employeeResponse = new EmployeeResponse();
        BeanUtils.copyProperties(employee, employeeResponse);
        return employeeResponse;
    }
}

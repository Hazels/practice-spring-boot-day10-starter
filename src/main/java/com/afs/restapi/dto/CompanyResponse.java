package com.afs.restapi.dto;

public class CompanyResponse {
    private Integer id;
    private String name;

    private Integer employeeNumbers;

    public CompanyResponse() {
    }

    public CompanyResponse(Integer id, String name, Integer employeeNumbers) {
        this.id = id;
        this.name = name;
        this.employeeNumbers = employeeNumbers;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEmployeeNumbers() {
        return employeeNumbers;
    }

    public void setEmployeeNumbers(Integer employeeNumbers) {
        this.employeeNumbers = employeeNumbers;
    }
}

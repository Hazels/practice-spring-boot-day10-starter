package com.afs.restapi.service;

import com.afs.restapi.dto.CompanyCreateRequest;
import com.afs.restapi.dto.CompanyResponse;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.mapper.CompanyMapper;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.afs.restapi.mapper.CompanyMapper.toEntity;
import static com.afs.restapi.mapper.CompanyMapper.toResponse;


@Service
public class CompanyService {
    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<CompanyResponse> getAll() {
        List<Company> companyList = companyRepository.findAll();
        return companyList.stream().map(
                CompanyMapper::toResponse).collect(Collectors.toList());
    }

    public List<CompanyResponse> getAll(Integer page, Integer pageSize) {
        List<Company> companyList = companyRepository.findAll(PageRequest.of(page, pageSize)).toList();
        return companyList.stream().map(
                CompanyMapper::toResponse).collect(Collectors.toList());    }

    public CompanyResponse findById(Integer companyId) {
        Company company = companyRepository.findById(companyId).orElseThrow(CompanyNotFoundException::new);
        return toResponse(company);
    }

    public CompanyResponse create(CompanyCreateRequest company) {

        return toResponse(companyRepository.save(toEntity(company)));
    }

    public void deleteCompany(Integer companyId) {
        Optional<Company> company = companyRepository.findById(companyId);
        company.ifPresent(companyRepository::delete);
    }

    public CompanyResponse update(Integer companyId, CompanyCreateRequest updatingCompany) {
        Company company = companyRepository.findById(companyId)
                .orElseThrow(CompanyNotFoundException::new);
        if (updatingCompany.getName() != null) {
            company.setName(updatingCompany.getName());
        }

        return toResponse(companyRepository.save(company));
    }

    public List<EmployeeResponse> getEmployees(Integer companyId) {
        List<Employee> employeeList = companyRepository.findById(companyId)
                .orElseThrow(CompanyNotFoundException::new)
                .getEmployees();
        return employeeList.stream().map(
                EmployeeMapper::toResponse).collect(Collectors.toList());
    }
}

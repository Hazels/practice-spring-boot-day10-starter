package com.afs.restapi.service;

import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.mapper.CompanyMapper;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.afs.restapi.mapper.EmployeeMapper.toEntity;
import static com.afs.restapi.mapper.EmployeeMapper.toResponse;

@Service
public class EmployeeService {

    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        List<Employee> employeeList = employeeRepository.findAllByStatusTrue();
        return employeeList.stream().map(
                EmployeeMapper::toResponse).collect(Collectors.toList());
    }

    public EmployeeResponse update(int id, EmployeeCreateRequest toUpdate) {
        Employee employee = employeeRepository.findByIdAndStatusTrue(id).orElseThrow(EmployeeNotFoundException::new);
        if (toUpdate.getAge() != null) {
            employee.setAge(toUpdate.getAge());
        }
        if (toUpdate.getSalary() != null) {
            employee.setSalary(toUpdate.getSalary());
        }

        employeeRepository.save(employee);
        return toResponse(employee);

    }

    public EmployeeResponse findById(int id) {
        Employee employee = employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new);
        return toResponse(employee);
    }

    public List<EmployeeResponse> findByGender(String gender) {
        List<Employee> employeeList =  employeeRepository.findByGenderAndStatusTrue(gender);
        return employeeList.stream().map(
                EmployeeMapper::toResponse).collect(Collectors.toList());
    }

    public List<EmployeeResponse> findByPage(int pageNumber, int pageSize) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        List<Employee> employeeList = employeeRepository.findAllByStatusTrue(pageRequest).toList();
        return employeeList.stream().map(
                EmployeeMapper::toResponse).collect(Collectors.toList());
    }

    public EmployeeResponse insert(EmployeeCreateRequest employeeCreateRequest) {
        employeeCreateRequest.setStatus(true);
        return toResponse(employeeRepository.save(toEntity(employeeCreateRequest)));
    }


    public void delete(int id) {
        Employee employee = toEntity(findById(id));
        employee.setStatus(false);
        employeeRepository.save(employee);
    }
}

package com.afs.restapi;

import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.afs.restapi.mapper.EmployeeMapper.toEntity;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;

@ExtendWith(SpringExtension.class)
class EmployeeServiceTest {

    @Mock
    EmployeeRepository employeeRepository;

    @InjectMocks
    EmployeeService employeeService;

    @Test
    void should_return_all_employees_when_find_all_given_employees() {
        // given
        Employee employee = new Employee(1, "Susan", 22, "Female", 7000, 100);
        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(employee);
        given(employeeRepository.findAllByStatusTrue()).willReturn(employees);

        // when
        List<EmployeeResponse> result = employeeService.findAll();

        // should
        assertThat(result, hasSize(1));
        assertThat(toEntity(result.get(0)).getId(), equalTo(employee.getId()));
        assertThat(toEntity(result.get(0)).getName(), equalTo(employee.getName()));
        assertThat(toEntity(result.get(0)).getAge(), equalTo(employee.getAge()));
        assertThat(toEntity(result.get(0)).getGender(), equalTo(employee.getGender()));
        assertThat(toEntity(result.get(0)).getCompanyId(), equalTo(employee.getCompanyId()));

    }

    @Test
    void should_update_only_age_and_salary_when_update_given_employee() {
        // given
        String originalName = "Susan";
        Integer newAge = 23;
        String originalGender = "Female";
        Integer newSalary = 10000;
        EmployeeCreateRequest toUpdateEmployee = new EmployeeCreateRequest( "Tom", newAge, "Male", newSalary, 100,true);

        given(employeeRepository.findByIdAndStatusTrue(1)).willReturn(
                Optional.of(new Employee(1, originalName, 22, originalGender, 5000, 100))
        );

        // when
        employeeService. update(1, toUpdateEmployee);
        Employee employee = employeeRepository.findByIdAndStatusTrue(1).get();
        // should
        assertThat(employee.getName(), equalTo(originalName));
        assertThat(employee.getAge(), equalTo(newAge));
        assertThat(employee.getGender(), equalTo(originalGender));
        assertThat(employee.getSalary(), equalTo(newSalary));
    }
}
